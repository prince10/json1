//
//  ViewController.m
//  json
//
//  Created by Prince on 29/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *collectedData;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView1;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@end

@implementation ViewController
@synthesize label1;
@synthesize tableView1;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString *jasondata=[[NSBundle mainBundle] pathForResource:@"jsontext" ofType:@"json"];
    NSData *data=[NSData dataWithContentsOfFile:jasondata];
    NSDictionary *jason=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    //NSLog(@"%@",jason);
//    
//    NSString *string= jason[@"base"];
//    NSLog(@"%@",string);
    
    
    
//    NSString *string1=jason[@"all"];
//    NSLog(@"%@",string1);
    
    
//    
//    NSDictionary *cloudDictionary=jason[@"clouds"];
//    NSLog(@"%@",cloudDictionary);
//    NSNumber *cloudNumber=cloudDictionary[@"all"];
//    NSLog(@"%@",cloudNumber);
    
    NSDictionary *coordDictionary=jason[@"coord"];
    NSNumber *lati=coordDictionary[@"lon"];
    
    NSLog(@"%@",coordDictionary);
    NSLog(@"%@",lati);
    NSNumber *longitude=coordDictionary[@"lat"];
    NSLog(@"%@",longitude);
    
    NSDictionary *mainDictionary=jason[@"main"];
    NSNumber *pressureNumber=mainDictionary[@"pressure"];
    NSNumber *maxTemp=mainDictionary[@"temp_max"];
    
    
    NSNumber *minTemp=mainDictionary[@"temp_min"];
    NSLog(@"%@",pressureNumber);
    NSLog(@"%@",maxTemp);
     NSLog(@"%@",minTemp);
    label1.text=[NSString stringWithFormat:@"pressure = %@, \n    maxtemp = %@, \n    mintemp = %@  , \nlatitude = %@ , \n longitude = %@ ",pressureNumber,maxTemp,minTemp,lati,longitude];
    
    
    
    
    collectedData=[[NSMutableArray alloc]init];
  // [ collectedData addObject:[NSNumber numberWithInt:pressureNumber ]];
    
    
    
    
    //NSLog(@"%@",mainDictionary);
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return collectedData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView1 dequeueReusableCellWithIdentifier:@"identity"];
    cell.textLabel.text=collectedData[indexPath.row];
    return cell;
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
